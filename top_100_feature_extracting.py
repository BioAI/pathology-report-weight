import heapq

import pandas

distance = pandas.read_csv("distance_feature.csv")
patient = pandas.read_csv("patient.csv")

dataset_folder = "data"
input_images = [
    dataset_folder + "/TCGA-2Z-A9J3-01Z-00-DX1.6B0E7DF6-733D-4007-84A3-E67BBC4966A6.svs/76001_40001_512.png",
    dataset_folder + "/TCGA-A3-3306-01Z-00-DX1.bfd320d3-f3ec-4015-b34a-98e9967ea80d.svs/53001_60001_512.png",
    dataset_folder + "/TCGA-A3-3328-01Z-00-DX1.8FA7DFCD-D6FD-4950-9E44-E5AB0B91E33D.svs/46001_7001_512.png",
    dataset_folder + "/TCGA-A4-A5DU-01Z-00-DX1.ACCD40B6-CC28-4E89-B9A9-9199F0541075.svs/17001_10001_512.png",
    dataset_folder + "/TCGA-UW-A7GP-01Z-00-DX1.C997CBA5-AF20-4CB3-9CB3-82AA65146EF5.svs/22001_69001_512.png"]
count = 0
for input_image in input_images:
    count += 1
    result_temp = []
    print(heapq.nsmallest(100, distance[input_image]))
    result = sorted(range(len(distance[input_image])), key=lambda i: distance[input_image][i])[:600]
    # result = map(distance[input_image].index, heapq.nsmallest(100, distance[input_image]))
    # print(str(list(result)))
    print("----------" + str(count) + "----------")
    for item in result:
        print(distance["file"][item][:12])
        for index, patient_temp in patient.iterrows():
            if distance["file"][item][:12] == patient_temp["bcr_patient_barcode"]:
                result_temp.append(patient_temp)
                break
    pandas.DataFrame(result_temp).to_csv(str(count) + "_patient.csv")
