import heapq
import os
import shutil
from collections import defaultdict

import pandas as pd
import tensorflow as tf
from keras.applications.vgg16 import VGG16
from keras.layers import *
from keras.layers import Conv2D
from keras.layers import Dense, Dropout, Flatten
from keras.models import Model
from keras.preprocessing.image import load_img, img_to_array
from skimage import transform


def convnet_model_():
    vgg_model = VGG16(weights=None, include_top=False)
    x = vgg_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(4096, activation='relu')(x)
    x = Dropout(0.6)(x)
    x = Dense(4096, activation='relu')(x)
    x = Dropout(0.6)(x)
    x = Lambda(lambda x_: tf.nn.l2_normalize(x, axis=1))(x)
    convnet_model = Model(inputs=vgg_model.input, outputs=x)
    return convnet_model


def deep_rank_model():
    convnet_model = convnet_model_()
    first_input = Input(shape=(224, 224, 3))
    first_conv = Conv2D(96, kernel_size=(8, 8), strides=(16, 16), padding='same')(first_input)
    first_max = MaxPool2D(pool_size=(3, 3), strides=(4, 4), padding='same')(first_conv)
    first_max = Flatten()(first_max)
    first_max = Lambda(lambda x: tf.nn.l2_normalize(x, axis=1))(first_max)

    second_input = Input(shape=(224, 224, 3))
    second_conv = Conv2D(96, kernel_size=(8, 8), strides=(32, 32), padding='same')(second_input)
    second_max = MaxPool2D(pool_size=(7, 7), strides=(2, 2), padding='same')(second_conv)
    second_max = Flatten()(second_max)
    second_max = Lambda(lambda x: tf.nn.l2_normalize(x, axis=1))(second_max)

    merge_one = concatenate([first_max, second_max])

    merge_two = concatenate([merge_one, convnet_model.output])
    emb = Dense(4096)(merge_two)
    l2_norm_final = Lambda(lambda x: tf.nn.l2_normalize(x, axis=1))(emb)

    final_model = Model(inputs=[first_input, second_input, convnet_model.input], outputs=l2_norm_final)

    return final_model


model = deep_rank_model()

# for layer in model.layers:
#     print (layer.name, layer.output_shape)

model.load_weights("deepranking-v2-150000.h5")

dataset = []

dataset_folder = "data"
for folder in os.listdir(dataset_folder):
    for file in os.listdir(dataset_folder + folder):
        if file[-12:] == 'boundary.png' or file[-8:] == 'gray.png':
            continue
        dataset.append(dataset_folder + folder + "/" + file)

print(dataset)

input_images = [
    dataset_folder + "/TCGA-2Z-A9J3-01Z-00-DX1.6B0E7DF6-733D-4007-84A3-E67BBC4966A6.svs/76001_40001_512.png",
    dataset_folder + "/TCGA-A3-3306-01Z-00-DX1.bfd320d3-f3ec-4015-b34a-98e9967ea80d.svs/53001_60001_512.png",
    dataset_folder + "/TCGA-A3-3328-01Z-00-DX1.8FA7DFCD-D6FD-4950-9E44-E5AB0B91E33D.svs/46001_7001_512.png",
    dataset_folder + "/TCGA-A4-A5DU-01Z-00-DX1.ACCD40B6-CC28-4E89-B9A9-9199F0541075.svs/17001_10001_512.png",
    dataset_folder + "/TCGA-UW-A7GP-01Z-00-DX1.C997CBA5-AF20-4CB3-9CB3-82AA65146EF5.svs/22001_69001_512.png"]

embeddings = []
embeddings_input = []
distance = defaultdict(list)

for image_item in dataset:
    image_temp = load_img(image_item)
    image_temp = img_to_array(image_temp).astype("float64")
    image_temp = transform.resize(image_temp, (224, 224))
    image_temp *= 1. / 255
    image_temp = np.expand_dims(image_temp, axis=0)
    embeddings.append(model.predict([image_temp, image_temp, image_temp])[0])

for input_image in input_images:
    image_temp = load_img(input_image)
    image_temp = img_to_array(image_temp).astype("float64")
    image_temp = transform.resize(image_temp, (224, 224))
    image_temp *= 1. / 255
    image_temp = np.expand_dims(image_temp, axis=0)
    embeddings_input.append(model.predict([image_temp, image_temp, image_temp])[0])

count = -1
for embedding_item_input in embeddings_input:
    count += 1
    for embedding_item in embeddings:
        distance[input_images[count]].append(
            sum([(embedding_item_input[idx] - embedding_item[idx]) ** 2 for idx in
                 range(len(embedding_item_input))]) ** (0.5))

distance["file_name"] = dataset.copy()

pd.DataFrame(distance).to_csv("distance.csv")

count_1 = 0
for input_image in input_images:
    result = map(distance[input_image].index, heapq.nsmallest(10, distance[input_image]))
    count_2 = 0
    for i in result:
        shutil.copy(dataset[i], "result/" + str(count_1) + "_" + str(count_2) + '.png')
        count_2 += 1
    count_1 += 1
