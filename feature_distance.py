import heapq
import shutil
from collections import defaultdict

import pandas

data = pandas.read_csv("feature_526_float.csv", dtype=float)
# data = data.dropna(axis=0, how='any')
data_str = pandas.read_csv("feature_526.csv")
data_norm = (data - data.mean()) / (data.std())

dataset_folder = "data"
input_images = [
    dataset_folder + "/TCGA-2Z-A9J3-01Z-00-DX1.6B0E7DF6-733D-4007-84A3-E67BBC4966A6.svs/76001_40001_512.png",
    dataset_folder + "/TCGA-A3-3306-01Z-00-DX1.bfd320d3-f3ec-4015-b34a-98e9967ea80d.svs/53001_60001_512.png",
    dataset_folder + "/TCGA-A3-3328-01Z-00-DX1.8FA7DFCD-D6FD-4950-9E44-E5AB0B91E33D.svs/46001_7001_512.png",
    dataset_folder + "/TCGA-A4-A5DU-01Z-00-DX1.ACCD40B6-CC28-4E89-B9A9-9199F0541075.svs/17001_10001_512.png",
    dataset_folder + "/TCGA-UW-A7GP-01Z-00-DX1.C997CBA5-AF20-4CB3-9CB3-82AA65146EF5.svs/22001_69001_512.png"]

input_image_index = []
count_1 = 0
for input_image in input_images:
    slide = input_image.split("/")[-2]
    region = input_image.split("/")[-1].split(".")[0]
    for index, item in data_str.iterrows():
        if item['folder'] == slide and item["file_name"] == region:
            input_image_index.append(index)
            break

distance = defaultdict(list)
for index in range(len(input_images)):
    for row_index, embedding_item in data.iterrows():
        # print(embedding_item.keys())
        # for value in embedding_item.keys():
        #     print(value)
        distance[input_images[index]].append(
            sum([(data_norm[key][input_image_index[index]] - data_norm[key][row_index]) ** 2 for key in
                 embedding_item.keys()]) ** (0.5))

for input_image in input_images:
    result = map(distance[input_image].index, heapq.nsmallest(10, distance[input_image]))
    count_2 = 0
    for i in result:
        shutil.copy(dataset_folder + "/" + data_str['folder'][i] + "/" + data_str['file_name'][i] + '.png',
                    "result_feature/" + str(count_1) + "_" + str(count_2) + '.png')
        count_2 += 1
    count_1 += 1

distance["file"] = data_str['folder'] + "/" + data_str['file_name']
pandas.DataFrame(distance).to_csv("distance_feature.csv")
