opencv-python==4.1.1.26
numpy==1.16.2
pandas==0.23.4
six==1.12.0
scikit-image==0.14.1
tensorflow==1.14.0
Keras==2.2.4
pyradiomics==3.0