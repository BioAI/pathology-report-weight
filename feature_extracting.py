import cv2
import numpy
import os
import pandas
import tensorflow as tf
from radiomics import featureextractor
from skimage.color import rgb2hed
from skimage.measure import regionprops

from segmentation_algorithm import SegmentationModel

model = SegmentationModel()
graph = tf.get_default_graph()

count = 0
feature_total = []
dataset_folder = "data"
for folder in os.listdir(dataset_folder):
    for file in os.listdir(dataset_folder + folder):
        if file[-12:] == 'boundary.png' or file[-8:] == 'gray.png':
            continue
        file_name = file[:-4]
        count += 1
        print(count, folder, file_name)

        with graph.as_default():
            mask = model.predict(dataset_folder + folder + "/" + file_name + ".png")
        # print(mask)
        region_image = model.water_image(mask)
        while numpy.sum(region_image == 1) == 0:
            region_image = region_image - 1
            region_image[region_image < 0] = 0
        region_image = region_image.astype(numpy.uint8)
        cv2.imwrite(dataset_folder + folder + "/" + file_name + "_boundary.png", region_image)

        image = cv2.imread(dataset_folder + folder + "/" + file_name + ".png")
        img_gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        image = rgb2hed(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))[:, :, 0]
        cv2.imwrite(dataset_folder + folder + "/" + file_name + "_gray.png", img_gray)

        # extractor = featureextractor.RadiomicsFeatureExtractor()
        settings = {}
        settings['binWidth'] = 20
        settings['sigma'] = [1, 2, 3]

        # Instantiate the extractor
        extractor = featureextractor.RadiomicsFeatureExtractor(
            **settings)  # ** 'unpacks' the dictionary in the function call
        feature = extractor.execute(dataset_folder + folder + "/" + file_name + "_gray.png",
                                    dataset_folder + folder + "/" + file_name + "_boundary.png")

        image_label = region_image
        image = cv2.imread(dataset_folder + folder + "/" + file_name + ".png")
        image = rgb2hed(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))[:, :, 0]  # cv2默认为bgr顺序

        region_props = regionprops(image_label, image)

        area = []
        perimeter = []
        bounding_rectangle_width = []
        bounding_rectangle_height = []
        equivalent_diameter = []
        ellipse_major_axis = []
        ellipse_minor_axis = []
        circularity = []
        aspect_ratio = []
        roundness = []
        solidity = []
        feret_diameter = []
        mean = []
        median = []
        standard_deviation = []
        skewness = []
        kurtosis = []

        for item in region_props:
            if item.label != 255:
                area.append(item.area)
                perimeter.append(item.perimeter)
                bounding_rectangle_width.append(item.bbox[3] - item.bbox[1])
                bounding_rectangle_height.append(item.bbox[2] - item.bbox[0])
                equivalent_diameter.append(item.equivalent_diameter)
                ellipse_major_axis.append(item.major_axis_length)
                ellipse_minor_axis.append(item.minor_axis_length)
                circularity.append(4 * numpy.pi * item.area / pow(item.perimeter, 2))
                aspect_ratio.append((item.bbox[3] - item.bbox[1]) / (item.bbox[2] - item.bbox[0]))
                solidity.append(item.solidity)
                roundness.append(4 * numpy.pi * item.area / pow(item.perimeter, 2))
                feret_diameter.append(item.perimeter / numpy.pi)

                temp = item.intensity_image[item.intensity_image != 0]
                mean.append(numpy.mean(temp))
                median.append(numpy.median(temp))
                standard_deviation.append(numpy.std(temp))
                skewness.append(numpy.mean((temp - numpy.mean(temp)) ** 3) / numpy.std(temp))
                kurtosis.append(numpy.mean((temp - numpy.mean(temp)) ** 4) / (numpy.var(temp) ** 2))

        feature_temp = {}
        feature_temp["folder"] = folder
        feature_temp["file_name"] = file_name
        feature_temp["Area"] = numpy.mean(area)
        feature_temp["Perimeter"] = numpy.mean(perimeter)
        feature_temp["Equivalent_Diameter"] = numpy.mean(equivalent_diameter)
        feature_temp["Bounding_Rectangle_Width"] = numpy.mean(bounding_rectangle_width)
        feature_temp["Bounding_Rectangle_Height"] = numpy.mean(bounding_rectangle_height)
        feature_temp["Ellipse_Major_Axis"] = numpy.mean(ellipse_major_axis)
        feature_temp["Ellipse_Minor_Axis"] = numpy.mean(ellipse_minor_axis)
        feature_temp["Circularity"] = numpy.mean(circularity)
        feature_temp["Aspect_Ratio"] = numpy.mean(aspect_ratio)
        feature_temp["Roundness"] = numpy.mean(roundness)
        feature_temp["Solidity"] = numpy.mean(solidity)
        feature_temp["Feret_Diameter"] = numpy.mean(feret_diameter)
        feature_temp["Mean"] = numpy.mean(mean)
        feature_temp["Median"] = numpy.mean(median)
        feature_temp["Standard_Deviation"] = numpy.mean(standard_deviation)
        feature_temp["Skewness"] = numpy.mean(skewness)
        feature_temp["Kurtosis"] = numpy.mean(kurtosis)

        feature_temp["Correlation"] = feature["original_glcm_Correlation"]
        feature_temp["Cluster_Shade"] = feature["original_glcm_ClusterShade"]
        feature_temp["Cluster_Prominence"] = feature["original_glcm_ClusterProminence"]
        feature_temp["Energy"] = feature["original_firstorder_Energy"]
        feature_temp["Entropy"] = feature["original_firstorder_Entropy"]
        feature_temp["Gray_Level_Non_Uniformity"] = feature["original_glrlm_GrayLevelNonUniformity"]
        feature_temp["Run_Length_Non_Uniformity"] = feature["original_glrlm_RunLengthNonUniformity"]
        feature_temp["Low_Gray_Level_Run_Emphasis"] = feature["original_glrlm_LowGrayLevelRunEmphasis"]
        feature_temp["High_Gray_Level_Run_Emphasis"] = feature["original_glrlm_HighGrayLevelRunEmphasis"]
        feature_temp["Short_Run_Emphasis"] = feature["original_glrlm_ShortRunEmphasis"]
        feature_temp["Short_Run_High_Gray_Level_Emphasis"] = feature["original_glrlm_ShortRunHighGrayLevelEmphasis"]
        feature_temp["Long_Run_Low_Gray_Level_Emphasis"] = feature["original_glrlm_LongRunLowGrayLevelEmphasis"]
        feature_temp["Long_Run_High_Gray_Level_Emphasis"] = feature["original_glrlm_LongRunHighGrayLevelEmphasis"]
        feature_total.append(feature_temp.copy())

pandas.DataFrame(feature_total).to_csv('feature_526.csv')
